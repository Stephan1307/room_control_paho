#include "room_control_paho.hpp"
#include "tinyosc.h"
#include <boost/bind/bind.hpp>
#include <sstream>
#include <boost/system/error_code.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/error.hpp>
#include <algorithm>

using namespace boost::placeholders;
using namespace boost::asio;
using namespace std::chrono;
using asio::error::basic_errors::operation_aborted;

typedef room_control_paho self;
typedef mqtt_node_paho super;

using std::move;
using std::size_t;
using std::unique_ptr;
using std::shared_ptr;
using std::make_unique;
using std::make_shared;
using std::string;
using std::cout;
using std::endl;
using boost::system::error_code;
using boost::asio::ip::tcp;
using std::to_string;
using std::map;
using std::vector;
using std::find;

typedef std::shared_ptr<const std::string> StrPtr;
typedef std::shared_ptr<tcp::socket> SockPtr;

const std::chrono::milliseconds self::debounce_time(100);

template <typename... Args>
auto share_str(Args&&... args) -> decltype(make_shared<string>(std::forward<Args>(args)...)) {
  return make_shared<string>(std::forward<Args>(args)...);
}

// Here we have to make sure the elements align with the SCENE_* enum
const vector<string> self::scene_names {
    "RESETLIGHT",
    "RESETLIGHTOFF",
    "PREGAME",
    "STARTGAME",
    "FUSESSOLVED",
    "STAFFSOLVED",
    "SWITCHSOLVED",
    "TUBESSOLVED",
    "RRSOLVED",
    "ALARM",
    "ALARMSOLVED",
    "TABLESOLVED",
    "GUESTSOLVED",
    "PICSSOLVED",
    "LPSOLVED",
    "DICESSTART",
    "DICESSOLVED",
    "STICKERSOLVED",
    "BEERSOLVED"
};

const vector<string> self::riddle_names {
    "PREGAME",
    "STARTGAME",
    "FUSES",
    "STAFF",
    "SWITCH",
    "TUBES",
    "RESTROOM",
    "ALARM",
    "GUEST",
    "TABLE",
    "KARAOKE",
    "PICS",
    "LAUNCHPAD",
    "STICKER",
    "DICES",
    "SAFE",
    "BEER",
    "DOOR"
};

const vector<vector<int>> self::switches {
    vector<int> {
        32,
        33,
        25,
        26,
        27,
        18,
         4,
        13
    },
    vector<int> {
        32,
        33,
        25,
        26,
        27,
        18,
         4,
        13
    }
};

const vector<vector<int>> self::magnets {
    vector<int> {
        23,
        22,
        19,
         5,
        14,
        12,
         2,
        15
    },
    vector<int> {
        23,
        22,
        19,
         5,
        14,
        12,
         2,
        15
    }
};

const string self::topic_switches = "switches";
const string self::topic_magnets = "magnets";
const string self::topic_signal = "signal";
const string self::topic_gameevent = "gameevent";
const string self::topic_adminsolve = "adminsolve";
const string self::signal_reset = "RESET";
const string self::signal_activate = "ACTIVATE";
const string self::signal_resetlight = "RESETLIGHT";
const string self::signal_resetlightoff = "RESETLIGHTOFF";
const string self::signal_adminsolve = "ADMINSOLVE";
const string self::gameevent_solved = "SOLVED";

#define CUE_LAUNCHPAD "/cue/435/start"

self::room_control_paho(
    boost::asio::io_context &ioc,
    std::string node_name,
    std::string broker_uri,
    std::string node_launchpad,
    std::string node_karaoke,
    std::string node_beer,
    std::vector<std::unique_ptr<firmata::io_base>> io_bases,
    std::string qlab_host,
    unsigned qlab_port,
    unsigned ola_universe,
    std::vector<ola::DmxBuffer> scenes
)
    : super(ioc, move(node_name), move(broker_uri)),
      full_topic_switches(full_topic(topic_switches)),
      full_topic_magnets(full_topic(topic_magnets)),
      full_topic_adminsolve(full_topic(topic_adminsolve)),
      full_topic_launchpad_signal(node_launchpad +"/"+ topic_signal),
      full_topic_launchpad_gameevent(node_launchpad +"/"+ topic_gameevent),
      full_topic_karaoke_signal(node_karaoke +"/"+ topic_signal),
      full_topic_beer_signal(node_beer +"/"+ topic_signal),
      full_topic_beer_gameevent(node_beer +"/"+ topic_gameevent),
      riddles(N_RIDDLES, 0),
      io_bases(move(io_bases)),
      arduinos(),
      debouncer(make_unique<firmata::debounce>(ioc, debounce_time)),
      qlab_host(move(qlab_host)),
      qlab_port(qlab_port),
      qlab_resolver(ioc),
      ola_universe(ola_universe),
      ola_client(ola::client::StreamingClient::Options()),
      sockets(),
      next_sockid(0),
      timers(),
      next_timerid(0),
      scenes(move(scenes)),
      standalones_deactivated(false)
{
    assert(riddle_names.size() == N_RIDDLES);
    assert(riddles.size() == N_RIDDLES);
    assert(scene_names.size() == N_SCENES);
    if (io_bases.size() > 2) {
        io_bases.resize(2);
    }
    ola::DmxBuffer blackout;
    blackout.Blackout();
    scenes.resize(N_SCENES, blackout);
}

firmata::pin &self::sw(int ardi, int swi) {
    assert(ardi < arduinos.size());
    assert(ardi < switches.size());
    assert(swi < switches[ardi].size());
    return arduinos[ardi]->pin(switches[ardi][swi]);
}

firmata::pin &self::mag(int ardi, int magi) {
    assert(ardi < arduinos.size());
    assert(ardi < magnets.size());
    assert(magi < magnets[ardi].size());
    return arduinos[ardi]->pin(magnets[ardi][magi]);
}

std::string self::sw_json() {
    std::ostringstream ss;
    ss << "[";
    for (size_t i = 0; i < arduinos.size(); i++) {
        if (i) { ss << ","; }
        ss << "[";
        for (size_t j = 0; j < switches[i].size(); j++) {
            if (j) { ss << ","; }
            ss << sw(i, j).state();
        }
        ss << "]";
    }
    ss << "]";
    return ss.str();
}

std::string self::mag_json() {
    std::ostringstream ss;
    ss << "[";
    for (size_t i = 0; i < arduinos.size(); i++) {
        if (i) { ss << ","; }
        ss << "[";
        for (size_t j = 0; j < magnets[i].size(); j++) {
            if (j) { ss << ","; }
            ss << mag(i, j).value();
        }
        ss << "]";
    }
    ss << "]";
    return ss.str();
}

void self::publish_switches() {
    string json = sw_json();
    cout << debug_prefix() << "switches: " << json << endl;
    mqtt_client.publish(full_topic_switches, move(json), 0, true);
}

void self::publish_magnets() {
    string json = mag_json();
    cout << debug_prefix() << "magnets: " << json << endl;
    mqtt_client.publish(full_topic_magnets, move(json), 0, true);
}

void self::try_activate_launchpad() {
    try {
        mqtt_client.publish(full_topic_launchpad_signal, signal_activate);
    }
    catch (const mqtt::exception &e) {
        cout << debug_prefix() << "Cannot activate launchpad: " << e.what() << endl;
    }
}

void self::try_activate_karaoke() {
    try {
        mqtt_client.publish(full_topic_karaoke_signal, signal_activate);
    }
    catch (const mqtt::exception &e) {
        cout << debug_prefix() << "Cannot activate karaoke: " << e.what() << endl;
    }
}

void self::try_activate_beer() {
    try {
        mqtt_client.publish(full_topic_beer_signal, signal_activate);
    }
    catch (const mqtt::exception &e) {
        cout << debug_prefix() << "Cannot activate beer: " << e.what() << endl;
    }
}

void self::deactivate_launchpad() {
    mqtt_client.publish(full_topic_launchpad_signal, signal_reset);
}

void self::deactivate_karaoke() {
    mqtt_client.publish(full_topic_karaoke_signal, signal_reset);
}

void self::deactivate_beer() {
    mqtt_client.publish(full_topic_beer_signal, signal_reset);
}

void self::try_deactivate_launchpad() {
    try {
        deactivate_launchpad();
    }
    catch (const mqtt::exception &e) {
        cout << debug_prefix() << "Cannot deactivate launchpad: " << e.what() << endl;
    }
}

void self::try_deactivate_karaoke() {
    try {
        deactivate_karaoke();
    }
    catch (const mqtt::exception &e) {
        cout << debug_prefix() << "Cannot deactivate karaoke: " << e.what() << endl;
    }
}

void self::try_deactivate_beer() {
    try {
        deactivate_beer();
    }
    catch (const mqtt::exception &e) {
        cout << debug_prefix() << "Cannot deactivate beer: " << e.what() << endl;
    }
}

void self::try_solve_launchpad() {
    try {
        mqtt_client.publish(full_topic_launchpad_signal, signal_adminsolve);
    }
    catch (const mqtt::exception &e) {
        cout << debug_prefix() << "Cannot solve launchpad: " << e.what() << endl;
    }
}

void self::try_solve_karaoke() {
    try {
        mqtt_client.publish(full_topic_karaoke_signal, signal_adminsolve);
    }
    catch (const mqtt::exception &e) {
        cout << debug_prefix() << "Cannot solve karaoke: " << e.what() << endl;
    }
}

void self::try_solve_beer() {
    try {
        mqtt_client.publish(full_topic_beer_signal, signal_adminsolve);
    }
    catch (const mqtt::exception &e) {
        cout << debug_prefix() << "Cannot solve beer: " << e.what() << endl;
    }
}

void self::startup(bool publish) {
    sockets.clear();
    timers.clear();
    for (size_t i = 0; i < arduinos.size(); i++) {
        for (size_t j = 0; j < magnets[i].size(); j++) {
            if (i == 1 && j == 0) {
                mag(i, j).value(0);
            }
            else {
                mag(i, j).value(1);
            }
        }
    }
    if (publish) {
        try_deactivate_launchpad();
        try_deactivate_karaoke();
        try_deactivate_beer();
        try_publish_magnets();
    }
    for (int &riddle : riddles) {
        riddle = 0;
    }
    try_send_dmx(scenes[SCENE_RESETLIGHT]);
    std::cout << debug_prefix() << "All magnets set to HIGH and DMX set to " << scene_names[SCENE_RESETLIGHT] << std::endl;
}

void self::clean_exit() {
    typedef map<self::sid, SockPtr>::iterator siter;
    qlab_resolver.cancel();
    sockets.clear();
    timers.clear();
    ola_client.Stop();
    for (size_t i = 0; i < arduinos.size(); i++) {
        for (size_t j = 0; j < magnets[i].size(); j++) {
            mag(i, j).value(0);
        }
    }
    debouncer.reset();
    arduinos.clear();
    io_bases.clear();
    super::clean_exit();
}

static std::string generate_osc_packet(const std::string &cmd) {
    char osc_msg[512];
    osc_msg[0] = 0300;
    int32_t tosc_len = int32_t(tosc_writeMessage(osc_msg + 1, sizeof(osc_msg) - 2, cmd.data(), "") + 1);
    assert(tosc_len >= 1);
    osc_msg[tosc_len++] = 0300;
    return std::string(osc_msg, osc_msg + tosc_len);
}

self::sid self::create_socket() {
    assert(!sockets.count(next_sockid));
    sockets[next_sockid] = make_shared<tcp::socket>(ioc);
    return next_sockid++;
}

void self::close_socket(sid sockid) {
    assert(sockets.count(sockid));
    sockets.erase(sockid);
}

self::tid self::create_timer() {
    if (timers.count(next_timerid)) {
        throw std::runtime_error("out of timers");
    }
    timers[next_timerid] = make_shared<asio::steady_timer>(ioc);
    return next_timerid++;
}

void self::close_timer(tid timerid) {
    assert(timers.count(timerid));
    timers.erase(timerid);
}

void self::qlab_handle_write(const error_code &ec, size_t bytes, StrPtr cue, StrPtr osc, sid sockid) {
    (void)bytes;
    (void)osc;
    if (!ec) {
        std::cout << debug_prefix() << "Cue sent to qlab: " << *cue << endl;
        error_code shutdown_ec;
        sockets[sockid]->shutdown(tcp::socket::shutdown_both, shutdown_ec);
        if (shutdown_ec) {
            cout << debug_prefix() << "Failed to shutdown socket " << sockid << endl;
        }
    }
    else if (ec.value() != operation_aborted) {
        std::cout << debug_prefix() << "QLab write error: " << ec.message() << endl;
    }
    close_socket(sockid);
}

void self::qlab_handle_connect(const error_code &ec, const tcp::resolver::endpoint_type &endpoint, StrPtr cue, sid sockid) {
    if (!ec) {
        std::cout << debug_prefix() << "QLab connected" << endl;
        StrPtr osc = share_str(generate_osc_packet(*cue));
        boost::asio::async_write(*(sockets[sockid]), asio::buffer(*osc),
                                 boost::bind(&self::qlab_handle_write, this,
                                             asio::placeholders::error, asio::placeholders::bytes_transferred,
                                             cue, osc, sockid));
    }
    else {
        if (ec.value() != operation_aborted) {
            std::cout << debug_prefix() << "QLab connect error: " << ec.message() << endl;
        }
        close_socket(sockid);
    }
}

void self::qlab_handle_resolve(const error_code &ec, tcp::resolver::results_type endpoints, StrPtr cue) {
    if (!ec) {
        std::cout << debug_prefix() << "QLab resolved" << endl;
        sid sockid = create_socket();
        boost::asio::async_connect(*(sockets[sockid]), endpoints, boost::bind(&self::qlab_handle_connect, this, asio::placeholders::error,
                                                                 asio::placeholders::endpoint, cue, sockid));
    }
    else if (ec.value() != operation_aborted) {
        std::cout << debug_prefix() << "QLab resolve error: " << ec.message() << endl;
    }
}

void self::send_cue(StrPtr cue) {
    qlab_resolver.async_resolve(qlab_host, to_string(qlab_port),
                                boost::bind(&self::qlab_handle_resolve, this,
                                asio::placeholders::error, asio::placeholders::results, cue));
}

bool self::interpret_message(mqtt::const_message_ptr msg) {
    if (super::interpret_message(msg)) {
        return true;
    }
    if (msg->get_topic() == full_topic_signal) {
        if (msg->to_string() == signal_reset) {
            std::cout << debug_prefix() << "Reset" << std::endl;
            startup();
            return true;
        }
        if (msg->to_string() == signal_resetlight) {
            std::cout << debug_prefix() << "Setting scene resetlight" << std::endl;
            try_send_dmx(scenes[SCENE_RESETLIGHT]);
            return true;
        }
        if (msg->to_string() == signal_resetlightoff) {
            std::cout << debug_prefix() << "Setting scene resetlightoff" << std::endl;
            try_send_dmx(scenes[SCENE_RESETLIGHTOFF]);
            return true;
        }
        return false;
    }
    if (msg->get_topic() == full_topic_adminsolve) {
        vector<string>::const_iterator rdliter = find(
                    riddle_names.cbegin(), riddle_names.cend(), msg->to_string());
        if (rdliter != riddle_names.cend()) {
            riddle_solved(rdliter - riddle_names.cbegin(), true);
            return true;
        }
        cout << debug_prefix() << "Unknown riddle '" << msg->to_string() << "'" << endl;
        return false;
    }
    if (msg->get_topic() == full_topic_launchpad_gameevent) {
        if (msg->to_string() == gameevent_solved) {
            riddle_solved(RIDDLE_LAUNCHPAD, true);
            return true;
        }
        return false;
    }
    if (msg->get_topic() == full_topic_beer_gameevent) {
        if (msg->to_string() == gameevent_solved) {
            riddle_solved(RIDDLE_BEER, true);
            return true;
        }
        return false;
    }
    return false;
}

void self::try_publish_switches() {
    try {
        publish_switches();
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "Cannot publish switches: " << e.what() << std::endl;
    }
}

void self::try_publish_magnets() {
    try {
        publish_magnets();
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "Cannot publish magnets: " << e.what() << std::endl;
    }
}

void self::try_send_dmx(const ola::DmxBuffer &buffer) {
    if (!ola_client.SendDmx(ola_universe, buffer)) {
        std::cout << debug_prefix() << "Cannot send DMX scene" << std::endl;
    }
}

void self::riddle_solved(unsigned riddle, bool adminsolve) {
    assert(riddle < riddle_names.size());
    assert(riddle < riddles.size());
    if (riddles[riddle]) {
        cout << debug_prefix() << "Riddle " << riddle_names[riddle] << " already solved" << std::endl;
    }
    else {
        if (adminsolve) {
            cout << debug_prefix() << "Riddle " << riddle_names[riddle] << " solved (ADMINSOLVE)" << std::endl;
        }
        else {
            cout << debug_prefix() << "Riddle " << riddle_names[riddle] << " solved" << std::endl;
        }
        switch(riddle) {
        case RIDDLE_PREGAME:
            try_send_dmx(scenes[SCENE_PREGAME]);
            break;
        case RIDDLE_STARTGAME:
            send_cue(share_str("/cue/400/start"));
            try_send_dmx(scenes[SCENE_STARTGAME]);
            break;
        case RIDDLE_FUSES: {
            mag(0, 0).value(0);
            try_publish_magnets();
            send_cue(share_str("/cue/405/start"));
            tid timerid = create_timer();
            timers[timerid]->expires_after(chrono::seconds(2));
            timers[timerid]->async_wait([this, timerid] (const boost::system::error_code& ec) {
                if (!ec) {
                    cout << debug_prefix() << "Setting DMX scene " << scene_names[SCENE_FUSESSOLVED] << endl;
                    try_send_dmx(scenes[SCENE_FUSESSOLVED]);
                }
                close_timer(timerid);
            });
            break;
        }
        case RIDDLE_STAFF:
            mag(0, 1).value(0);
            try_publish_magnets();
            try_send_dmx(scenes[SCENE_STAFFSOLVED]);
            send_cue(share_str("/cue/410/start"));
            break;
        case RIDDLE_SWITCH:
            mag(0, 2).value(0);
            try_publish_magnets();
            try_send_dmx(scenes[SCENE_SWITCHSOLVED]);
            // Hier wird der cue 405 von oben gestoppt. (407, da Gruppe in qlab)
            send_cue(share_str("/cue/407/stop"));
            send_cue(share_str("/cue/415/start"));
            break;
        case RIDDLE_TUBES:
            mag(0, 3).value(0);
            try_publish_magnets();
            try_send_dmx(scenes[SCENE_TUBESSOLVED]);
            send_cue(share_str("/cue/417/stop"));
            send_cue(share_str("/cue/420/start"));
            break;
        case RIDDLE_RESTROOM:
            mag(0, 4).value(0);
            try_publish_magnets();
            try_send_dmx(scenes[SCENE_RRSOLVED]);
            send_cue(share_str("/cue/423/start"));
            break;
        case RIDDLE_ALARM: {
            send_cue(share_str("/cue/455/start"));
            try_send_dmx(scenes[SCENE_ALARM]);
            tid timerid = create_timer();
            timers[timerid]->expires_after(chrono::seconds(10));
            timers[timerid]->async_wait([this, timerid] (const boost::system::error_code& ec) {
                if (!ec) {
                    cout << debug_prefix() << "Setting DMX scene " << scene_names[SCENE_ALARMSOLVED] << endl;
                    try_send_dmx(scenes[SCENE_ALARMSOLVED]);
                }
                close_timer(timerid);
            });
            break;
        }
        case RIDDLE_GUEST: {
            mag(0, 5).value(0);
            try_publish_magnets();
            try_send_dmx(scenes[SCENE_GUESTSOLVED]);
            send_cue(share_str("/cue/425/start")); /*Baustellen_atmo in qlab eher starten lassen (20 Sekunden) & leiser machen.*/
            tid timerid = create_timer();
            timers[timerid]->expires_after(chrono::seconds(120));
            timers[timerid]->async_wait([this, timerid] (const boost::system::error_code& ec) {
                if (!ec) {
                    const string cue = "/cue/482/start";
                    cout << debug_prefix() << "Setting cue " << cue << endl;
                    send_cue(share_str(cue));
                }
                close_timer(timerid);
            });
            break;
        }
        case RIDDLE_TABLE:
            mag(0, 6).value(0);
            try_publish_magnets();
            try_send_dmx(scenes[SCENE_TABLESOLVED]);
            send_cue(share_str("/cue/440/start"));
            try_activate_karaoke();
            break;
        case RIDDLE_KARAOKE:
            try_send_dmx(scenes[SCENE_DICESSOLVED]);
            /* Gewinner Sound einfügen oder gibt's das schon im Javascript?! */
            /* send_cue(share_str("/cue/430/start")); */
            if (adminsolve) {
                try_solve_karaoke();
            }
            break;
        case RIDDLE_PICS:
            mag(0, 7).value(0);
            try_publish_magnets();
            try_send_dmx(scenes[SCENE_PICSSOLVED]);
            send_cue(share_str("/cue/430/start"));
            try_activate_launchpad();
            break;
        case RIDDLE_LAUNCHPAD: {
            send_cue(share_str("/cue/435/start"));
            try_send_dmx(scenes[SCENE_ALARM]);
            tid timerid = create_timer();
            timers[timerid]->expires_after(chrono::seconds(21));
            timers[timerid]->async_wait([this, timerid] (const boost::system::error_code& ec) {
                if (!ec) {
                    cout << debug_prefix() << "Setting DMX scene " << scene_names[SCENE_DICESSOLVED] << endl;
                    try_send_dmx(scenes[SCENE_DICESSOLVED]);
                    tid timerid = create_timer();
                    timers[timerid]->expires_after(chrono::seconds(99));
                    timers[timerid]->async_wait([this, timerid] (const boost::system::error_code& ec) {
                        if (!ec) {
                            const string cue = "/cue/482/start";
                            cout << debug_prefix() << "Setting cue " << cue << endl;
                            send_cue(share_str(cue));
                        }
                        close_timer(timerid);
                    });
                }
                close_timer(timerid);
            });
            if (adminsolve) {
                try_solve_launchpad();
            }
            break;
        }
        case RIDDLE_STICKER:
            mag(1, 0).value(1);
            try_publish_magnets();
            /*send_cue(share_str("/cue/445/start"));*/ /*geeigneten Sound suchen*/
            try_send_dmx(scenes[SCENE_STICKERSOLVED]);
            break;
        case RIDDLE_DICES:
            mag(1, 1).value(0);
            try_publish_magnets();
            try_send_dmx(scenes[SCENE_DICESSOLVED]);
            send_cue(share_str("/cue/445/start"));
            break;
        case RIDDLE_SAFE: {
            mag(1, 2).value(0);
            try_publish_magnets();
            tid timerid = create_timer();
            timers[timerid]->expires_after(chrono::seconds(15));
            timers[timerid]->async_wait([this, timerid] (const boost::system::error_code& ec) {
                if (!ec) {
                    const string cue = "/cue/495/start";
                    cout << debug_prefix() << "Setting cue" << share_str(cue) << endl;
                    send_cue(share_str(cue));
                    tid timerid = create_timer();
                    timers[timerid]->expires_after(chrono::seconds(12));
                    timers[timerid]->async_wait([this, timerid] (const boost::system::error_code& ec) {
                        if (!ec) {
                            cout << debug_prefix() << "try_activate_beer" << endl;
                            try_activate_beer();
                        }
                        close_timer(timerid);
                    });
                }
                close_timer(timerid);
            });
            break;
        }
        case RIDDLE_BEER: {
            mag(1, 3).value(0);
            try_publish_magnets();
            try_send_dmx(scenes[SCENE_BEERSOLVED]);
            send_cue(share_str("/cue/425/stop"));
            send_cue(share_str("/cue/450/start"));
            tid timerid = create_timer();
            timers[timerid]->expires_after(chrono::seconds(10));
            timers[timerid]->async_wait([this, timerid] (const boost::system::error_code& ec) {
                if (!ec) {
                    cout << debug_prefix() << "Setting DMX scene " << scene_names[SCENE_DICESSOLVED] << endl;
                    try_send_dmx(scenes[SCENE_DICESSOLVED]);
                }
                close_timer(timerid);
            });
            if (adminsolve) {
                try_solve_beer();
            }
            break;
        }
        case RIDDLE_DOOR:
            mag(1, 4).value(0);
            try_publish_magnets();
            break;
        default:
            assert(!"Riddle switch default case reached");
        }
        riddles[riddle] = 1;
    }
}

void self::switch_state_changed(int ard, int sw, int state) {
    std::cout << debug_prefix() << "ard " << ard << " sw " << sw << " is " << state << std::endl;
    try_publish_switches();
    if (!state) {
        int r = N_RIDDLES;
        switch (ard) {
        case 0:
            switch (sw) {
            case 0:
                r = RIDDLE_FUSES;
                break;
            case 1:
                r = RIDDLE_STAFF;
                break;
            case 2:
                r = RIDDLE_SWITCH;
                break;
            case 3:
                r = RIDDLE_TUBES;
                break;
            case 4:
                r = RIDDLE_RESTROOM;
                break;
            case 5:
                r = RIDDLE_GUEST;
                break;
            case 6:
                r = RIDDLE_TABLE;
                break;
            case 7:
                r = RIDDLE_PICS;
                break;
            default:
                assert(!"The default case of the arduino 0 pin switch was reached");
            }
            break;
        case 1:
            switch (sw) {
            case 0:
                r = RIDDLE_STICKER;
                break;
            case 1:
                r = RIDDLE_DICES;
                break;
            case 2:
                r = RIDDLE_SAFE;
                break;
            case 3: /* Should be actually triggered by mqtt signal */
                r = RIDDLE_BEER;
                break;
            case 4:
                r = RIDDLE_DOOR;
                break;
            // Reserved switch pins
            case 5:
            case 6:
            case 7:
                break;
            default:
                assert(!"The default case of the arduino 1 pin switch was reached");
            }
            break;
        default:
            assert(!"The default case of the arduino switch was reached");
        }
        if (r != N_RIDDLES) {
            riddle_solved(r);
        }
        else {
            cout << debug_prefix() << "No riddle defined for this switch" << endl;
        }
    }
}

void self::connected_action() {
    std::cout << debug_prefix() << "Subscribing to topic '" << full_topic_adminsolve << "'" << std::endl;
    mqtt_client.subscribe(full_topic_adminsolve, 0);
    std::cout << debug_prefix() << "Subscribing to topic '" << full_topic_launchpad_gameevent << "'" << std::endl;
    mqtt_client.subscribe(full_topic_launchpad_gameevent, 0);
    std::cout << debug_prefix() << "Subscribing to topic '" << full_topic_beer_gameevent << "'" << std::endl;
    mqtt_client.subscribe(full_topic_beer_gameevent, 0);
    std::cout << debug_prefix() << "Publishing switches json on topic '" << full_topic_switches << "'" << std::endl;
    publish_switches();
    std::cout << debug_prefix() << "Publishing magnets json on topic '" << full_topic_magnets << "'" << std::endl;
    publish_magnets();
    if (!standalones_deactivated) {
        cout << debug_prefix() << "Deactivating standalone riddles" << endl;
        deactivate_launchpad();
        deactivate_karaoke();
        deactivate_beer();
        standalones_deactivated = true;
    }
    super::connected_action();
}

void self::start() {
    std::cout << debug_prefix() << "Connecting arduinos ..." << std::endl;
    for (std::unique_ptr<firmata::io_base> &iob : io_bases) {
        arduinos.push_back(make_unique<firmata::client>(*iob));
    }
    std::cout << debug_prefix() << "Setting pins ..." << std::endl;
    for (size_t i = 0; i < arduinos.size(); i++) {
        for (size_t j = 0; j < switches[i].size(); j++) {
            sw(i, j).mode(firmata::pullup_in);
            debouncer->on_state_changed(sw(i, j), boost::bind(&self::switch_state_changed, this, i, j, _1));
        }
        for (size_t j = 0; j < magnets[i].size(); j++) {
            mag(i, j).mode(firmata::digital_out);
        }
    }
    std::cout << debug_prefix() << "Pins modes set" << std::endl;
    if (!ola_client.Setup()) {
        // std::cout << debug_prefix() << "Cannot connect to OLA server!" << std::endl;
        throw std::runtime_error("ola connection failed");
    }
    else {
        std::cout << debug_prefix() << "Connected to OLA server." << std::endl;
    }
    startup(false);
    super::start();
}

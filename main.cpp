#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>
#include "room_control_paho.hpp"
#include <memory>
#include "firmata/serial_port.hpp"
#include "boost/system/system_error.hpp"
#include "boost/system/error_code.hpp"
#include <chrono>
#include "cmake_config.h"
#include <string>
#include <algorithm>

using namespace std;

#define HELP "help"
#define VERSION "version"
#define CFG "config"

#define NODE "mqtt.node"
#define BROKER "mqtt.broker"
#define LP "mqtt.launchpad"
#define KAR "mqtt.karaoke"
#define BEER "mqtt.beer"
#define QLAB_HOST "qlab.host"
#define QLAB_PORT "qlab.port"
#define ARD_A_DEV "arduino.a.dev"
#define ARD_A_BAUD "arduino.a.baud"
#define ARD_B_DEV "arduino.b.dev"
#define ARD_B_BAUD "arduino.b.baud"
#define OLA_UNI "ola.universe"
#define OLA_SCENES "ola.scenes"
#define LIST_SCENES "list-scenes"

static std::unique_ptr<firmata::serial_port> create_serial(boost::asio::io_context &ioc, std::string device, unsigned baud) {
    std::unique_ptr<firmata::serial_port> ptr(std::make_unique<firmata::serial_port>(ioc, std::move(device)));
    ptr->set(static_cast<firmata::baud_rate>(baud));
    return ptr;
}

#include <chrono>
int main(int argc, char **argv)
{
    try {
        namespace po = boost::program_options;
        using std::cout;
        using std::cerr;
        using std::endl;
        using std::ifstream;
        typedef std::string str;
        typedef unsigned uns;

        po::options_description generic("Generic options");
        generic.add_options()
            (HELP ",h", "produce help message")
            (VERSION ",v", "print version string")
            (LIST_SCENES ",L", "list all dmx scene names")
            (CFG ",g", po::value<str>()->value_name("FILE"), "configuration file")
        ;

        po::options_description config("Configuration");
        config.add_options()
            (BROKER ",b", po::value<str>()->value_name("URI")->default_value("localhost"), "mqtt broker uri")
            (NODE ",n", po::value<str>()->value_name("NAME")->default_value("control"), "this node")
            (LP ",l", po::value<str>()->value_name("NAME")->default_value("launchpad"), "launchpad node")
            (KAR ",k", po::value<str>()->value_name("NAME")->default_value("karaoke"), "karaoke node")
            (BEER ",s", po::value<str>()->value_name("NAME")->default_value("beer"), "beer node")
            (ARD_A_DEV ",A", po::value<str>()->value_name("DEV")->default_value("/dev/ttyUSB0"), "arduino A device")
            (ARD_A_BAUD ",a", po::value<uns>()->value_name("BAUD")->default_value(115200), "arduino A baud rate")
            (ARD_B_DEV ",B", po::value<str>()->value_name("DEV"), "arduino B device")
            (ARD_B_BAUD ",b", po::value<uns>()->value_name("BAUD")->default_value(115200), "arduino B port")
            (QLAB_HOST ",Q", po::value<str>()->value_name("HOST")->default_value("qlab"), "qlab hostname")
            (QLAB_PORT ",q", po::value<uns>()->value_name("PORT")->default_value(53000), "qlab port")
            (OLA_UNI ",o", po::value<uns>()->value_name("NUM")->default_value(1), "ola universe")
            (OLA_SCENES ",d", po::value<str>()->value_name("FILE"), "csv file with dmx scenes")
        ;

        po::options_description cmdline_opts;
        cmdline_opts.add(generic).add(config);

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, cmdline_opts), vm);

        str progname = ((argc >= 1) ? argv[0] : "NONAME");

        if (vm.count(HELP)) {
            cout << "Usage: " << progname << " [OPTION]...\n" << cmdline_opts << endl;
            return 0;
        }

        if (vm.count(VERSION)) {
            cout << progname << " version " << PROJECT_VERSION  << endl;
            return 0;
        }

        if (vm.count(LIST_SCENES)) {
            for (const std::string &s : room_control_paho::scene_names) {
                std::cout << s << std::endl;
            }
            return 0;
        }

        if (vm.count(CFG)) {
            const str &cfg_file = vm[CFG].as<str>();
            ifstream cfg_stream(cfg_file);
            if (cfg_stream) {
                po::store(boost::program_options::parse_config_file(cfg_stream, config), vm);
            }
            else {
                cerr << "Error: Cannot open config file " << cfg_file << endl;
                return 1;
            }
        }

        po::notify(vm);

        ola::DmxBuffer blackout;
        blackout.Blackout();
        std::vector<ola::DmxBuffer> scenes(room_control_paho::N_SCENES, blackout);

        if (vm.count(OLA_SCENES)) {
            const str &scene_file = vm[OLA_SCENES].as<str>();
            ifstream scene_stream(scene_file);
            using std::string;
            using std::size_t;
            using std::getline;
            using std::vector;
            using ola::DmxBuffer;
            using std::find;
            if (scene_stream) {
                string line;
                size_t first_comma;
                string name;
                string rest;
                while (getline(scene_stream, line)) {
                    first_comma = line.find(',');
                    if (first_comma != string::npos) {
                        name = line.substr(0, first_comma);
                        rest = line.substr(first_comma + 1);
                        vector<string>::const_iterator scene = find(
                                    room_control_paho::scene_names.cbegin(), room_control_paho::scene_names.cend(), name);
                        if (scene != room_control_paho::scene_names.cend()) {
                            size_t scene_index = scene - room_control_paho::scene_names.cbegin();
                            assert(scene_index < scenes.size());
                            if (!scenes[scene_index].SetFromString(rest)) {
                                cerr << "Scene " << *scene << " cannot be parsed" << endl;
                                return 2;
                            }
                        }
                    }
                }
                std::cout << "Scene file " << scene_file << " parsed successfully:\n";
                for (size_t i = 0; i < room_control_paho::N_SCENES; i++) {
                    std::cout << room_control_paho::scene_names[i] << ": ";
                    if(scenes[i] == blackout) {
                        std::cout << "BLACKOUT";
                    }
                    else {
                        std::cout << scenes[i].ToString();
                    }
                    std::cout << '\n';
                }
                std::cout << std::flush;
            }
            else {
                cerr << "Error: Cannot open scene file " << scene_file << endl;
                return 1;
            }
        }

        boost::asio::io_context ioc;
        std::vector<std::unique_ptr<firmata::io_base>> io_bases;

        std::cout << "Connecting to " << vm[ARD_A_DEV].as<str>() << " ..." << std::endl;

        bool not_exist;
        do {
            not_exist = false;
            try {
                io_bases.push_back(
                    create_serial(ioc, vm[ARD_A_DEV].as<str>(), vm[ARD_A_BAUD].as<uns>())
                );
            }
            catch (const boost::system::system_error &e) {
                if (e.code().value() == boost::system::errc::no_such_file_or_directory) {
                    not_exist = true;
                    std::this_thread::sleep_for(std::chrono::milliseconds(10));
                }
                else {
                    throw e;
                }
            }
        }
        while (not_exist);

        std::cout << "Done." << std::endl;

        if (vm.count(ARD_B_DEV)) {
            std::cout << "Connecting to " << vm[ARD_B_DEV].as<str>() << " ..." << std::endl;
            do {
                not_exist = false;
                try {
                    io_bases.push_back(
                        create_serial(ioc, vm[ARD_B_DEV].as<str>(), vm[ARD_B_BAUD].as<uns>())
                    );
                }
                catch (const boost::system::system_error &e) {
                    if (e.code().value() == boost::system::errc::no_such_file_or_directory) {
                        not_exist = true;
                        std::this_thread::sleep_for(std::chrono::milliseconds(10));
                    }
                    else {
                        throw e;
                    }
                }
            }
            while (not_exist);
            std::cout << "Done." << std::endl;
        }

        room_control_paho control(
                    ioc, vm[NODE].as<str>(), vm[BROKER].as<str>(),
                    vm[LP].as<str>(), vm[KAR].as<str>(), vm[BEER].as<str>(), std::move(io_bases),
                    vm[QLAB_HOST].as<str>(), vm[QLAB_PORT].as<uns>(), vm[OLA_UNI].as<uns>(),
                    std::move(scenes));
        control.start();
        ioc.run();
        return 0;
    }
    /* catch (const boost::system::system_error &e) {
        std::cerr << "Terminating due to boost exception: " << e.code().value() << std::endl;
        return 1;
    } */
    catch (const std::exception &e) {
        std::cerr << "Terminating due to exception: " << e.what() << std::endl;
        return 1;
    }
}

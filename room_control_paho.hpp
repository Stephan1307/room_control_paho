#ifndef ROOM_CONTROL_PAHO_HPP
#define ROOM_CONTROL_PAHO_HPP

#include "mqtt_node_paho.hpp"
#include "firmata/client.hpp"
#include "firmata/io_base.hpp"
#include "firmata/debounce.hpp"
#include <vector>
#include <ola/DmxBuffer.h>
// #include <ola/Logging.h>
#include <ola/client/StreamingClient.h>
#include <chrono>
#include <memory>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/system/error_code.hpp>
#include <map>

class room_control_paho : public mqtt_node_paho
{
    /**
     * @brief socket id type
     */
    typedef unsigned sid;

    /**
     * @brief timer id type
     */
    typedef unsigned tid;

    /**
     * 0 to 11 used for normal riddles
     * hier habe ich jetzt alle Zustände eingeführt, die über Buttons vom Betreuer ausgelöst werden können.
     * Oder neue Spielzustände wie bspw. Launchpadsolved (auch Launchpadsolved muss der Betreuer selbst senden können, falls die Gruppe es einfach nicht schafft)
     */
    enum {
        RIDDLE_PREGAME,
        RIDDLE_STARTGAME,
        RIDDLE_FUSES,
        RIDDLE_STAFF,
        RIDDLE_SWITCH,
        RIDDLE_TUBES,
        RIDDLE_RESTROOM,
        RIDDLE_ALARM, // no switch pin
        RIDDLE_GUEST,
        RIDDLE_TABLE,
        RIDDLE_KARAOKE,
        RIDDLE_PICS,
        RIDDLE_LAUNCHPAD,
        RIDDLE_STICKER,
        RIDDLE_DICES,
        RIDDLE_SAFE,
        RIDDLE_BEER,
        RIDDLE_DOOR,
        N_RIDDLES
    };

    static const std::chrono::milliseconds debounce_time;
    static const std::vector<std::vector<int>> switches;
    static const std::vector<std::vector<int>> magnets;
    static const std::string topic_switches;
    static const std::string topic_magnets;
    static const std::string topic_signal;
    static const std::string topic_gameevent;
    static const std::string topic_adminsolve;
    static const std::string signal_reset;
    static const std::string signal_activate;
    static const std::string signal_resetlight;
    static const std::string signal_resetlightoff;
    static const std::string gameevent_solved;
    static const std::string signal_adminsolve;
    static const std::vector<std::string> riddle_names;

    const std::string full_topic_switches;
    const std::string full_topic_magnets;
    const std::string full_topic_adminsolve;
    const std::string full_topic_launchpad_signal;
    const std::string full_topic_launchpad_gameevent;
    const std::string full_topic_karaoke_signal;
    const std::string full_topic_beer_signal;
    const std::string full_topic_beer_gameevent;
    std::vector<int> riddles;
    std::vector<std::unique_ptr<firmata::io_base>> io_bases;
    std::vector<std::unique_ptr<firmata::client>> arduinos;
    std::unique_ptr<firmata::debounce> debouncer;
    std::string qlab_host;
    const unsigned qlab_port;
    boost::asio::ip::tcp::resolver qlab_resolver;
    const unsigned ola_universe;
    ola::client::StreamingClient ola_client;
    std::map<sid, std::shared_ptr<boost::asio::ip::tcp::socket>> sockets;
    sid next_sockid;
    std::map<tid, std::shared_ptr<boost::asio::steady_timer>> timers;
    tid next_timerid;
    std::vector<ola::DmxBuffer> scenes;
    bool standalones_deactivated;

    // int pin_to_switch_index(int arduino, int pin);
    void switch_state_changed(int arduino, int sw, int state);

    /**
     * @brief get switch pin
     * @param ardi - arduino index
     * @param swi - switch index
     * @return switch pin reference
     */
    firmata::pin &sw(int ardi, int swi);

    /**
     * @brief get magnet pin
     * @param ardi - arduino index
     * @param magi - magnet index
     * @return magnet pin reference
     */
    firmata::pin &mag(int ardi, int magi);

    std::string sw_json();
    std::string mag_json();

    void publish_switches();
    void publish_magnets();

    void try_publish_switches();
    void try_publish_magnets();
    void try_send_dmx(const ola::DmxBuffer &buffer);

    void try_activate_launchpad();
    void try_activate_karaoke();
    void try_activate_beer();

    void deactivate_launchpad();
    void deactivate_karaoke();
    void deactivate_beer();

    void try_deactivate_launchpad();
    void try_deactivate_karaoke();
    void try_deactivate_beer();

    void try_solve_launchpad();
    void try_solve_karaoke();
    void try_solve_beer();

    void startup(bool publish = true);

    void send_cue(std::shared_ptr<const std::string> cue);

    sid create_socket();
    void close_socket(sid sockid);

    tid create_timer();
    void close_timer(tid timerid);

    void qlab_handle_resolve(const boost::system::error_code &ec,
                             boost::asio::ip::tcp::resolver::results_type results,
                             std::shared_ptr<const std::string> cue);

    void qlab_handle_connect(const boost::system::error_code &ec,
                             const boost::asio::ip::tcp::resolver::endpoint_type &endpoint,
                             std::shared_ptr<const std::string> cue,
                             sid sockid);

    void qlab_handle_write(const boost::system::error_code &ec, std::size_t bytes,
                           std::shared_ptr<const std::string> cue, std::shared_ptr<const std::string> osc,
                           sid sockid);

    void connected_action() override;
    bool interpret_message(mqtt::const_message_ptr msg) override;
    void clean_exit() override;

    void riddle_solved(unsigned riddle, bool adminsolve = false);


public:
    /* hier habe ich alle DMX Szenen eingeführt */
    enum {
        SCENE_RESETLIGHT,
        SCENE_RESETLIGHTOFF,
        SCENE_PREGAME,
        SCENE_STARTGAME,
        SCENE_FUSESSOLVED,
        SCENE_STAFFSOLVED,
        SCENE_SWITCHSOLVED,
        SCENE_TUBESSOLVED,
        SCENE_RRSOLVED,
        SCENE_ALARM,
        SCENE_ALARMSOLVED,
        SCENE_TABLESOLVED,
        SCENE_GUESTSOLVED,
        SCENE_PICSSOLVED,
        SCENE_LPSOLVED,
        SCENE_DICESSTART,
        SCENE_DICESSOLVED,
        SCENE_STICKERSOLVED,
        SCENE_BEERSOLVED,
        N_SCENES
    };
    static const std::vector<std::string> scene_names;
    room_control_paho(boost::asio::io_context &ioc,
                      std::string node_name,
                      std::string broker_uri,
                      std::string node_launchpad,
                      std::string node_karaoke,
                      std::string node_beer,
                      std::vector<std::unique_ptr<firmata::io_base>> io_bases,
                      std::string qlab_host,
                      unsigned qlab_port,
                      unsigned ola_universe,
                      std::vector<ola::DmxBuffer> scenes = {});
    void start() override;
};

#endif // ROOM_CONTROL_PAHO_HPP
